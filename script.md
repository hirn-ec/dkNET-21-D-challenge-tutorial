### Title

Hi, and welcome to the tutorial “Finding resources for bioinformatics reuse on the HIRN Resource Browser”. 
This tutorial was first developed for the **DKNET/thesugarscience D-21 bioinformatics challenge**, but it should be generally useful for anyone who wants to learn about and access HIRN-related resources for a bioinformatics project, whether you're a beginner or more advanced. 

In a general overview, we’ll summarize the ideas behind the HIRN Resource Browser and where everything comes from. 

Then in part one, we’ll show you how to navigate the most relevant resource types within our catalog, Dataset and Technology. 
For a bioinformatics project, you might need to find the right datasets for in silico testing and something like machine learning. 
You might also be curious about potentially useful software packages or databases, which would be in our Technology catalog section. 

In the second part, because this tutorial is bioinformatics-focused, we go over our API and how you can access the catalog programmatically. 
The API will offer you a lot more power without you having to leave your code editor of choice. 
We'll even have a hands-on demonstration of the programmatic access using R to give you a sense of how you might want to sift through the resource data.

### Overview

So first we'll give you a general overview. 
Our HIRN Resource Browser is a catalog of resources that has been around for a couple of years now, though we are constantly evolving. 
The vast majority of these catalogued resources are produced or used in research funded by the Human Islet Research Network (HIRN). 
Resource information comes from curation by the HIRN curator (myself) and submission by investigators and their research staff in one of these consortiums. 
One important thing to note as well is that most resources are associated with a publication, so they are real, have been peer-reviewed, and are validated in some way. 
Our goal is to have all HIRN resources in one place. 
But to be clear, because this is a catalog, you might not be able to obtain the resource directly, such as by downloading it from our platform. 
In fact, I will be showing how you'd actually get Dataset resources in the next part.

### Part 1
#### (a) Datasets

You see here that we have six subtypes of datasets. 
The subtypes should be pretty recognizable except for, perhaps, Cellomics. 
If you view this category, you’ll see that it mostly includes imaging mass cytometry or FACS datasets.

It is very likely that when you’re looking for a dataset, you're going to want a certain subtype, so this current organization is a good starting point. 

For example, let's say you want a Transcriptomics dataset. 
You see here a listing of all the datasets we have cataloged as such. 
We're going to go visit a Transcriptomics dataset of high interest. 
You can see this dataset description, the HIRN contributor, the paper it's associate with, and you can see as well that it's been reused in a machine learning application. 
By the way, anyone can add an application by clicking "Rate this resource". 
We also add applications when we see applications in the wild, since it helps us track the impact of a resource. 
And really, applications provide more useful, real-life data about the resource. 
In fact, we really want people to go back and add application information, so if you end up using one of the datasets in your project, please go back and add an application entry. 
You can peruse the publication listed here for how this dataset was used as the train and test data in a Deep Neural Network model.
Personally, I think seing others' applications can be good for getting ideas or learning something new.
Reusing a dataset for hypothesis generation or producing something new involves creativity, and there are sometimes very obvious angles to approach a dataset, and sometimes less obvious ones.

For more information about the dataset, you can click on specifications, and you see here several things. 
You see important biosample characteristics, such as species, cell type, and kind of the biological process that this dataset describes. 
You also see something called a dataset value. 
Think of it as an assigned weight for how reusable this dataset is, on a range of 1 to 5, with 1 being low reusability and 5 being high reusability. 
This isn't available for all datasets because it's difficult to assign, and we haven't yet formalized the metric. 
It's a combination of whether we know the authors undertook a massive effort to generate this dataset as the main result, whether we've already seen reuse for the dataset in the wild, or whether this contains a lot of novel data in the current context of what's already been produced. 
Some of these factors that I've mentioned are relative and can change over time, so this weight can change, but we hope it can still provide guidance for narrowing down to the datasets you can spend more time looking at. 

Now let’s say that this does seem like a pretty valuable dataset, and you want to get it. 
There isn’t a download button anywhere. 
So what you do is go back and click the Canonical ID / Source, which is the accession and the link to where the data is actually deposited. 
If there is no ID or link, that means you will have to use the author Contact to obtain the dataset. 
The Contact will be a HIRN author who was involved in the project in some capacity. 
Or of course, you can go to the paper and look for another contact there. 
But communicating with a contact is much more time consuming than just going to where the data has been deposited, so especially if you're under time constraints, you may want to ignore datasets without an accession. 

Now we're back at the list of Transcriptomics datasets. 
You may be wondering how to filter through all the datasets. 
Let’s say you want to filter for human-sample datasets only. 
We do have a filter here, but right now you can only filter by the contributing HIRN consortium. 
Our UI is limited in that aspect right now, and we’re still working on refining the platform. 
But don’t worry -- in part 2, we’ll show how to get all the dataset data using the API and how to filter upon that.

#### (b) Technology

But before part two, we still have to cover Technology, here. 
You can ignore Assays and Device/Equipment, which are more relevant for web lab applications, and focus on the subtypes with more bioinformatics relevance, Code/Pipeline and Software/Database. You might wonder what's the difference between Code and Software. 
Well, so far within Code/Pipeline are scripts for processing original raw data, and many of them, especially the ad hoc scripts, are *probably* not going to be re-usable right out of the box. 

However, if you go to Software/Database, there are a couple of resouces that I'll highlight -- software platforms that are more useful out of the box, and that we've featured before. 
This includes PennAI, which is an auto machine learning platform. 
You can upload data and it will help you select a model and perform the learning. PennAI is pretty general-purpose, so it might be a good tool for hypothesis generation with low or no code.
Another tool that we featured is ImmuneML; it's a machine learning platform for adaptive immune receptor repertoires. 
This is a *little* more advanced and niche, but if this is your domain, it's worth seeing how it might be useful for you.

You should also notice a couple of databases where you can actually find more data. 
I would recommend PANC-DB, if you haven't already heard of it. 
There is a lot of reusable data there that you can download directly.

### Part 2

#### (a) Swagger

We're now at part two of the tutorial, where we cover our API and provide a hands-on demonstration in R. 
So, you can go ahead and click on the link on our menu called Data/API Access, and that will take you to an array of options. Here we have a standard REST API, which is the most mature and what we'll actually use. 
It's likely that you're going to be most familiar and comfortable with the REST API.  
We do have an RDF API and GRAPHQL API, but as you can see both are in beta.  

There's something here about an API key, but for just querying the data you do not need a key. 
Let's go ahead and click on MORE INFORMATION. 
Then click on VIEW API DOCUMENTATION, which will take you to our Swagger docs.

I'll review the endpoints with you before we get to the hands-on part.
You can pretty ignore the anything under the r underscore headings, as they're for submitting data. 
And that means that in fact, you only have one very simple GET endpoint to concern yourself about. 
If you scroll down to the Schemas section, that has the schemas for each of the resource types. 
You can go ahead and check out the metadata for Epigenomics -- and we've seen some of this metadata before in the UI. 
However, for certain resources, you can only get certain information through the API.

So of course for this REST API, you can use anything you want -- Python, Java, R, etc. 
But we'll go ahead and give you an example implementation in R.  

#### (b) Hands-on example with R

For this continuation of part 2, I have my Rstudio and an Rmarkdown document open, which we'll run step-by-step. 
Even if you've never used R before, or very minimally used R, you should still be able to follow along as we'll be doing quite basic tasks. 
This is meant to be a pretty beginner-friendly demo. 
And of course, this document is in our Gitlab repo, dkNET-21-D-Challenge-tutorial, so you can reference it after the video tutorial as well.   

So first, we'll make sure some packages are installed, and then we load them. 
The `Sourcery` package is a package that works with the Resource Browser API to get or submit data. 
Because it's an unofficial package, we're going to use devtools to install it. 
The `dplyr`, `tidyr`, and `purrr` packages are part of what's known as the `tidyverse` and will allow you to do the data sifting. 
If you've never used `tidyverse` before, this might double as a `tidyverse` introductory tutorial as well. 
Finally, the `GEOquery` package will allow you to download *most* of the dataset accessions directly. 

The next thing I do is pull the dataset resources. 
We're going to use a convenience function that simply wraps the main GET endpoint described previously to get "Transcriptomics" datasets.
You can, of course, later replace "Transcriptomics" with another type of dataset, or get more than one type.
To see the help documentation, type `?Sourcery::getResourceType` at the console.
This does do some stuff behind-the-scenes so you don't have to do parsing of the raw data yourself.
The return is converted to a `tibble`, which will help us filter and subset the collection of datasets in a nice `tidyverse` manner. 
We'll "glimpse" the data and go through what's available, and you might get a better idea compared to what you've seen via the UI.

This glimpse let's you see the attributes for the dataset and some sample values.
So notice that many dataset attributes are in list columns. 
That's because a dataset can have multiple sample Species, Cells, CellLines, Tissues, et cetera. 
We'll show you how to handle the list columns in a bit.
But, by the way, if you are truly new and this is more confusing than expected, we recommend this chapter from the Stanford Data Challenge Lab (DCL) course `https://dcl-prog.stanford.edu/list-columns.html`.

Now lets move on to some practical, power-browsing.
Previously in the UI, we pointed out a dataset value, though we said it might not be available for all datasets, and you can see that in the NA values here. 
Still, especially if you have limited time, you might just want to look at these "recommended" datasets, right? 
OK, so in the first example, how about we subset for datasets in the 4-5 range. 

Here's how to do that, and because we have limited screen space, we'll just select the name and value to view.
And hey, that's not bad. 
There are a total of 27 datasets, though only 10 are shown in the preview.
But maybe that's still too much to look at.
We modify the above to a combinatorial filter that additionally constrains selection by species.
The syntax for the filter upon the list column is a little more complicated, but basically we're just checking that "human" is one of the Species.
That appears to narrow down our selection to 16 datasets.

Now let's try something else in example two. 
Because dataset value is not available for a lot of datasets, especially newer datasets, you might want to focus instead on datasets that characterize T1D or T2D.
The way to do that is very similar to above.
With this and the previous example, you can implement a lot of different criteria options.

In our final example, let's see which are GEO-accessible datasets from the results above so we can try downloading one.
Here I filter for datasets that are actually in GEO. 
We'll select the first one to download.
Note that this downloads the *processed RNA-seq data*, which is available as a supplementary file.

You can see the dataset has about 23000 features and 8 samples.

Let's also take a look at some rows. 
Since the methods specified data processing using RSEM; the numerical values here are expected counts.

So further analysis of the data is beyond the scope of this tutorial, but I hope we've made things easier and given you some possibilities.
